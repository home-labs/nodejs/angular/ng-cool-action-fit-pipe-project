# NgCoolactionFitPipe

## Dependencies

- @cyberjs.on/coolaction-fit 1.0.0-2.

## Installing

	$ npm i @cyberjs.on/ng-coolaction-fit-pipe --save

## Usage

Include the module into `imports` metadata key of `NgModule` decorator in your application context, importing `NgCoolFilterPipeModule` from `@cjs.on/ng-cool-filter-pipe`, like that.

```typescript
import { NgCoolactionFitPipeModule } from '@cyberjs.on/ng-coolaction-fit-pipe';

@NgModule({
    imports: [
        NgCoolactionFitPipeModule
    ]
})
export class MyModule() { }
```

## Usage example with `*ngFor` directive

### Component source control

```typescript
import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

    objectCollection: object[];

    filterTerm: string;

    constructor() {
        this.objectCollection = [
            {
                name: 'First Name First Last Name',
                gender: 'male'
            },
            {
                name: 'Second Name Second Last Name',
                gender: 'male'
            },
            {
                name: 'Third Name Third Last Name',
                gender: 'famale'
            },
            {
                name: 'Fourty Name Fourty Last Name',
                gender: 'male'
            },
            {
                name: 'Fifty Name Fifty Last Name',
                gender: 'female'
            }
        ];

        this.filterTerm = '';
    }

}
```

### Template
```html
<input
  type="text"
  [(ngModel)]="filterTerm"
  [ngModelOptions]="{
    standalone: true
  }"
  />

<ul>
  <li *ngFor="let object of objectCollection | filterBy:filterTerm:'name'">
    {{ object.name }}
  </li>
</ul>
```

<!--stackedit_data:
eyJoaXN0b3J5IjpbMTk1MDc0NzI1MiwxMzc0Njg2NDc2LDI4Nz
A2NTYxMV19
-->

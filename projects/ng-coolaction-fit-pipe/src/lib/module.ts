import { NgModule } from '@angular/core';

import { FilterByPipe } from './pipes';


@NgModule({
    imports: [

    ],
    declarations: [
        FilterByPipe
    ],
    exports: [
        FilterByPipe
    ]
})
export class NgCoolactionFitPipeModule { }
